# Blue/green deployment to release multiple services simultaneously

In this example, we release a new version of service simultaneously using the blue/green deployment strategy. 
GKE LB in used as Ingress controller

- The initial infrastructure you will find [here](https://gitlab.com/Andrii_Kostenetskyi/go-hello-app/go-hello-app-infra)
- The application code and releases 
    - initial [code](https://gitlab.com/Andrii_Kostenetskyi/go-hello-app/go-hello-app) 
    - All releases will be pushed to [dockerhub](https://hub.docker.com/repository/docker/andriikostenetskyi/go-hello-app/)


# Steps to follow before the testing:

Before the execution of these steps, please, make sure that you have access to you k8s cluster on gke.
Authentificate on gcloud: 

```
    gcloud auth logi
```
Get the credentials:

```
    gcloud container clusters get-credentials docs-devops-cluster --region europe-central2 --project docs-devops
```


# General testing plan

### Deploy version 1 of application a and the ingress
$ ./scripts/management/install.sh site-a-v1 default

- where site-a-v1 - is the name of release which we install
- default - is the name of our namespace where the application will be installed 

##### Expected output:
```text
$ ./scripts/management/install.sh site-a-v1 default
Install default
NAME: site-a-v1
LAST DEPLOYED: Thu Jul  8 08:46:01 2021
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
Creating static resources.
```

###  Deploy ingress: 
$ k apply -f router.yaml

##### Expected output:
```text
$ k apply -f router.yaml 
Warning: networking.k8s.io/v1beta1 Ingress is deprecated in v1.19+, unavailable in v1.22+; use networking.k8s.io/v1 Ingress
ingress.networking.k8s.io/hello-gke-ing created
```
This will take some time, please, wait until you will see similar output
##### Expected output:
```text
$ k get ingress
NAME            CLASS    HOSTS   ADDRESS          PORTS   AGE
hello-gke-ing   <none>   *       34.149.101.148   80      3m32s
```


### Test if the deployment was successful
$ k get pods

##### Expected output:
```text
NAME                                        READY   STATUS    RESTARTS   AGE
site-a-v1-go-hello-world-6d86b57944-7gnmc   1/1     Running   0          2m39s
site-a-v1-go-hello-world-6d86b57944-zb7gn   1/1     Running   0          2m39s
```

by triggering the ingress IP you will get something like this:

 ![](screen/version1_pod1.png)

 ![](screen/version1_pod2.png)
 
In case if you see the following error:

 ![](screen/error.png)

**(!!!) It means that ingress is not ready yet...** 

### Deploy version 2 of application a 
$ ./scripts/management/install.sh site-a-v2 default

- where site-a-v2 - is the name of release which we install
- default - is the name of our namespace where the application will be installed 

##### Expected output:
```text
$ k get pods
NAME                                        READY   STATUS    RESTARTS   AGE
site-a-v1-go-hello-world-6d86b57944-7gnmc   1/1     Running   0          15m
site-a-v1-go-hello-world-6d86b57944-zb7gn   1/1     Running   0          15m
site-a-v2-go-hello-world-58f9cb5fdb-dmvw4   1/1     Running   0          5s
site-a-v2-go-hello-world-58f9cb5fdb-f6zz2   1/1     Running   0          6s
```

### Change the version 
- first change version of release on ingress
  
    $ sed -i 's/site-a-v1/site-a-v2/' router.yaml
  
- apply your changes 
  
    $ k apply -f router.yaml 

##### Expected output:
```text
$ kubectl apply -f router.yaml 
Warning: networking.k8s.io/v1beta1 Ingress is deprecated in v1.19+, unavailable in v1.22+; use networking.k8s.io/v1 Ingress
ingress.networking.k8s.io/hello-gke-ing configured
```

- now you should see that svc was changed to newer release:
$ kubectl describe ingress hello-gke-ing | grep /
  
##### Expected output:
```text
$ kubectl describe ingress hello-gke-ing | grep site-a
             /*   site-a-v2:80 (10.44.3.33:8080,10.44.3.34:8080,10.44.4.12:8080 + 1 more...)
```
where site-a-v2 name of our svc endpoint for newer release 

by triggering the ingress IP you will get something like this:

 ![](screen/version2_pod1.png)

 ![](screen/version2_pod2.png)

**It means that we are on new release...**

### Remove old version
$ ./scripts/management/uninstall.sh site-a-v1 default

##### Expected output:
```text
$ ./scripts/management/uninstall.sh site-a-v1 default
release "site-a-v1" uninstalled
Deleting static resources.
```

### Testing part
Go must be preinstaled on the system. 
Then executes smoke [test](https://github.com/bluehoodie/smoke)
$ go get github.com/bluehoodie/smoke

Execute the tests
$smoke -v -f ../smoke_test.yaml -u http://34.149.101.148 -p 80

##### Expected output:
```text
$ smoke -v -f ../smoke_test.yaml -u http://34.149.101.148 -p $PORT
[GIN] 2021/07/08 - 04:09:11 | 200 |      32.694µs |       127.0.0.1 | GET      "/hello"
✓	user_path_status_check
[GIN] 2021/07/08 - 04:09:11 | 200 |      83.183µs |       127.0.0.1 | GET      "/api/ping"
✓	httpbin_get_body
OK
```