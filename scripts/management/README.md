# Release Installation, Upgrade and Removal Scripts

This folder contains installation, upgrade and removal scripts for the different development releases of the blue/green POC
application on Kubernetes on gke

# Script
## Configuration
Two configuration files are present:
- `config.sh` contains the names of available releases.
- `release_config.sh` is used to create the release specific configuration, such as namespace and release names. 
                      It furthermore creates the paths to files used during the installation, the upgrade or the removal.

## Scripts
- `lib.sh`: a library shell scripts that contains useful functionality.
- `install.sh`: used to install the selected release.
- `upgrade.sh`: used to upgrade the selected release.
- `uninstall.sh`: used to remove the selected release.

These scripts first verify if a corresponding release exist, then get the configuration (location to the chart and to the values file).
Next, the install, upgrade or remove action is executed and finally, custom yaml files are applied.
The custom yaml files are located in values/static-yaml/ and are only considered by the specific release,
if the file name begins with the name of the release ($RELEASE\*.yaml).

# Releases

## Release: blue green infra
This is the main K8s development release.


