#!/bin/bash

scriptpath=$(dirname $0)
source $scriptpath/config.sh
source $scriptpath/lib.sh

scriptname=$(basename $0)
print_usage() {
  echo "USAGE: $scriptname RELASE"
  echo "  where"
  echo "    RELEASE is the name of the release to install"
}

if [ $# -lt 1 ] || [ $# -gt 3 ]; then
  print_error Invalid number of arguments
  print_usage
  exit 1
fi

release=$1
error_invalid_release $release
namespace=$2
error_invalid_namespace $namespace

source $scriptpath/release_config.sh

[ -f $scriptpath/configs/$release.sh ] && echo "Loading specific config file." && source $scriptpath/configs/$release.sh

helm uninstall --namespace $namespace $HELM_RELEASE
if [ $? -ne 0 ]; then
  print_error "Helm removal failed"
  exit 1
fi

echo "Deleting static resources."
for f in $scriptpath/../../values/static-yaml/$release*.yaml; do
  if [ -f $f ]; then
    kubectl -n $namespace delete -f $f
  fi
done

