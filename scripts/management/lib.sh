# Need to source config.sh.

debug() {
  if [ $DEBUG -eq 1 ]; then
    echo DEBUG: $@
  fi
}

print_error() {
  msg=$@
  echo ERROR: $msg!
}

error_exit() {
  exit_code=$1
  shift
  msg=$@
  print_error $msg
  exit $exit_code
}

error_invalid_release() {
  release=$1
  found=0
  for r in ${RELEASES[@]}; do
    if [[ "$release" == "$r" ]]; then
      found=1
    fi
  done

  if [ $found -ne 1 ]; then
    error "Invalid release: $release"
    print_all_releases
    exit 1
  fi
}

error_invalid_namespace() {
  namespace=$1
  found=0
  for n in ${NAMESPACES[@]}; do
    if [[ "$namespace" == "$n" ]]; then
      found=1
    fi
  done

  if [ $found -ne 1 ]; then
    error "Invalid NAMESPACE: $namespace"
    print_all_releases
    exit 1
  fi
}

print_all_releases(){
  echo "Valid releases are:"
  for r in ${RELEASES[@]}; do
    echo "  - $r"
  done
}

print_all_namespaces(){
  echo "Valid namespaces are:"
  for n in ${NAMESPACES[@]}; do
    echo "  - $n"
  done
}
