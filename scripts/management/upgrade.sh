#!/bin/bash

scriptpath=$(dirname $0)
source $scriptpath/config.sh
source $scriptpath/lib.sh

scriptname=$(basename $0)
print_usage() {
  echo "USAGE: $scriptname RELEASE"
  echo "  where"
  echo "    RELEASE is the name of the release to install"
}

if [ $# -lt 1 ] || [ $# -gt 3 ]; then
  print_error Invalid number of arguments
  print_usage
  exit 1
fi

release=$1
error_invalid_release $release
namespace=$2
error_invalid_namespace $namespace


source $scriptpath/release_config.sh

[ -f $scriptpath/configs/$release.sh ] && echo "Loading specific config file." && source $scriptpath/configs/$release.sh

helm upgrade --namespace $namespace $HELM_RELEASE $scriptpath/$CHART_PATH --values $scriptpath/$VALUES_FILE
if [ $? -ne 0 ]; then
  print_error "Helm upgrade failed"
  exit 1
fi

echo "Updating static resources."
for f in $scriptpath/../../values/static-yaml/$release*.yaml; do
  if [ -f $f ]; then
    kubectl -n $namespace apply -f $f
  fi
done

